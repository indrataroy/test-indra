SELECT user.id,user.userName, COALESCE(parent.userName,'NULL')  as ParentUserName
FROM user LEFT JOIN 
    (SELECT id,userName FROM user) as parent
    ON USER.parent = parent.id
