let words = ["kita", "atik", "aku", "tika", "uka", "makan", "kua", "kia"]
let result = [];
let arrIndex = 0;

while (words.length > 0) {
    let comparator = words[0]
    for (let j = 0; j < words.length; j++) {
        if (isAnagram(comparator, words[j])) {
            let word = words[j];
            if (!result[arrIndex])
                result[arrIndex] = []

            result[arrIndex].push(words[j])

            words = words.filter(a => a !== word);
            j--
        }
    }
    arrIndex++;
}

function isAnagram(first, second) {
    let temp1 = first.split("").sort().join("")
    let temp2 = second.split("").sort().join("")
    return temp1 == temp2 ? true : false;
}

console.log(result)