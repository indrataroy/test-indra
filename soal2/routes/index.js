
const { Router } = require('express');
const controllers = require('../controllers');

const router = Router();

router.get('/', (req, res) => res.send('Welcome'))

router.get('/detail/:id', controllers.detail);
router.get('/search/:key', controllers.search);

module.exports = router;