var mysql = require('mysql');

function insert(data = {}) {
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "test"
    });

    var date;
    date = new Date();
    date = date.getUTCFullYear() + '-' +
    ('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
    ('00' + date.getUTCDate()).slice(-2) + ' ' + 
    ('00' + date.getUTCHours()).slice(-2) + ':' + 
    ('00' + date.getUTCMinutes()).slice(-2) + ':' + 
    ('00' + date.getUTCSeconds()).slice(-2);

    con.connect(function (err) {
        if (err) throw err;
        var sql = "INSERT INTO api_log (end_point, parameter,time) VALUES ('" + data.endPoint + "', '" + data.parameter +"','"+date+"')";
        con.query(sql, function (err, result) {
            if (err) throw err;
        });
    });
}

module.exports={
    insert
}