const omdbapi = require('../http/omdapi')
const db = require('../db/mysql')

let query={
    endPoint:"",
    parameter:""
}

const search = async (req, res) => {
    try {
        const key=req.params.key
        await omdbapi.call(key,true).then(async (data)=>{
            res.send(data);
            setQuery(req);
            await db.insert(query)
        }).catch(error => {
            res.send(error)
        });
        res.end()
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
};

const detail = async (req, res) => {
    try {
        const id=req.params.id
        await omdbapi.call(id).then(async data=>{
            res.send(data)
            setQuery(req);
            await db.insert(query)
        }).catch(error => {
            throw error
        });
        res.end()
    } catch (error) {
        throw error
    }
};

function setQuery(req){
    query.endPoint=req.url.split('/')[1];
    query.parameter=req.url.split('/')[2];
}

module.exports = {
    search,
    detail
  };