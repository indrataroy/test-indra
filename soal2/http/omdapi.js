const https = require('https');
const apikey = "14eed537"

const options = {
    hostname: 'omdbapi.com',
    port: 443,
    path: '',
    method: 'GET'
}

function call(data, isSearch = false) {
    let temp='/?apikey=' + apikey;
    if (isSearch){
        options.path = temp+'&s='+data
    }else{
        options.path = temp+'&i='+data
    }

    return new Promise((resolve, reject) => {
        const req = https.request(options, res => {
            var { statusCode } = res;
            var contentType = res.headers['content-type'];
    
            let error;
    
            if (statusCode !== 200) {
                error = new Error('Request Failed.\n' +
                    `Status Code: ${statusCode}`);
            } else if (!/^application\/json/.test(contentType)) {
                error = new Error('Invalid content-type.\n' +
                    `Expected application/json but received ${contentType}`);
            }
    
            if (error) {
                console.error(error.message);
                res.resume();
            }
    
            res.setEncoding('utf8');
            let data = '';
    
            res.on('data', (chunk) => {
                data += chunk;
            });
    
            res.on('end', () => {
                try {
                    const parsedData = JSON.parse(data);
                    resolve(parsedData);
                } catch (e) {
                    reject(e.message);
                }
            });
        })
    
        req.on('error', error => {
            console.error(error)
        })
    
        req.end()
        return req.data;
    }); 
        
    
}

module.exports = {
    call
};
