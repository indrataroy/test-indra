const str = "makan (nasi)"

function findFirstStringInBracket(str) {
    const res = ""
    if (str.length > 0) {
        let indexFirstBracketFound = str.indexOf("(");
        let indexClosingBracketFound = str.indexOf(")");
        if (indexFirstBracketFound >= 0 && indexClosingBracketFound >= 0) {
            let wordBetweenBracket = str.substring(indexFirstBracketFound + 1, indexClosingBracketFound)
            return wordBetweenBracket
        }
    }
    return res;
}

console.log(findFirstStringInBracket(str))