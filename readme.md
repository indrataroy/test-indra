# Dkatalis Test

this app is nodejs app for test

## Prerequisite: install Node.js
If you haven’t already installed Node, download and install Node.js here https://nodejs.org/en/download/. For best results, use the latest LTS (long-term support) release of Node.js.

## Answer, Installation & start

### 1st Quiz
the answer in soal1.sql

### 2nd Quiz
```bash
cd soal2
npm install
node index.js
```

need to create mysqldb with configuration like,
`{
    host: "localhost",
    user: "root",
    password: "",
    database: "test"
}`
or live version could be seen in https://test-indra.herokuapp.com/


### 3rd Quiz
the answer in soal3.js\
it can be start with
```bash
node soal3.sql
```

### 4th quiz
```bash
node soal4.sql
```

